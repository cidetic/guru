## Repositorio del Grupo de Usuarios de R de la [UNLu](http://unlu.edu.ar) (GURU)

El uso de **R** en ciancia es cada vez más extendido, ya que facilita uno de los 
pilares del método científico: la [reproducibilidad](https://es.wikipedia.org/wiki/Reproducibilidad_y_repetibilidad),
que se refiere a que un trabajo de investigación se pueda reproducir ([mas información](https://kbroman.org/steps2rr/)). 

Por ello desde el [CIDETIC](https://cidetic.unlu.edu.ar/) promovemos el uso de **R**. Durante agosto y septiembre de 
2018 dictamos un primer curso introductorio de **R** y esperamos repetirlo muchas veces más. Queremos que *R* se 
convierta en una herramieta accesible para la mayor cantidad posible de estudiantes y docentes.

Este repositorio nace, entonces, como una de las iniciativa para promover el uso de **R** en la [UNLu](http://unlu.edu.ar). 
Aquí podrán encontrar [scripts de ejemplos](https://gitlab.com/cidetic/guru/tree/master/R_code) usados en cursos, 
[presentaciones](https://gitlab.com/cidetic/guru/tree/master/Presentaciones), 
archivos de [datos](https://gitlab.com/cidetic/guru/tree/master/data), así como también 
**[consultas](https://gitlab.com/cidetic/guru/issues)**.

Las **[consultas](https://gitlab.com/cidetic/guru/issues)** se realizan en la sección issues del repositorio.
Las consultas deben estar referidas a problemas a resolver con **R**, problemas de instalación, paquetes de **R**,
errores en scripts, u otros problemas relacionados a **R** o [RStudio](https://www.rstudio.com/).

Consultas sobre el repositorio dirigirlas a [Marcos Angelini](mailto:angelini75@gmail.com), 
[Andres Gordano](andresgiordano.unlu@gmail.com) o [Agustín Marrone](mailto:agustinhmarrone@gmail.com) 
y sobre el CIDETIC a [Gabriel Tolosa](mailto:tolosoft@gmail.com).